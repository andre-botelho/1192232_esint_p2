package esinf.csvparser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Fixed Width Table class.
 * <p>Represents a table that remains fixed in width but not height.</p>
 * @author 1192232
*/

public class FixedWidthTable<T> {
    /**
     * Matrix data.
     */
    List<T> matrixData;
    /**
     * Matrix width.
     */
    int width;
    /**
     * Matrix height.
     */
    int height;

    /**
     * Create a table without data, with a certain width.
     * @param w Table width.
     * @throws IllegalArgumentException See complete constructor.
     */
    public FixedWidthTable(int w) throws IllegalArgumentException{
        this(w, new ArrayList<T>());
    }

    /**
     * Create a new table.
     * @param width Width of the table, in case the table is empty it can be null, in order for the width to be automatically inferred.
     * @param tableData Table data.
     * @throws IllegalArgumentException If the tableData is non-empty and either width is null or it's size is not a multiple of the width.
     */
    private FixedWidthTable(int width, List<T> tableData) throws IllegalArgumentException{
        this.matrixData = tableData;
        this.width = width;
        this.height = 0;
    }

    /**
     * Adds a line to the table.
     * @param line Line to add. (If width == null, the width will be auto inferred)
     * @return True iff the line was added.
     * @throws IllegalArgumentException If the line size is incorrect.
     */
    public boolean addLine(List<T> line) throws IllegalArgumentException {
        if(line.size() != width) throw new IllegalArgumentException("Line size incorrect");
        if (matrixData.addAll(line)) {
            this.height++;
            return true;
        } else return false;
    }

    /**
     * Returns an unmodifiable list of containing a line from the table.
     * @param l Table line.
     * @return An unmodifiable list containing all the elements of the table at line l.
     */
    public List<T> get(Integer l) {
        return Collections.unmodifiableList(matrixData.subList(width*l, width*(l+1)));
    }

    /**
     * Returns the height of the table.
     * @return Height of the matrix.
     */
    public Integer getHeight() {
        return height;
    }
}
