package esinf.resolution;

import esinf.util.LineException;
import esinf.graph.Graph;
import esinf.graph.UndirectedMapGraph;
import esinf.graph.UndirectedMatrixGraph;
import esinf.util.Pair;

import java.io.IOException;
import java.util.*;

public class ExerciseResponse {
        UndirectedMatrixGraph<String, Boolean> p_friendshipGraph;
        UndirectedMapGraph<String, Boolean> p_cityBorders;
        Map<String, String> p_userCities;
        Graph<String, Double> p_cityDistances;
        Map<String, Integer> p_numberOfUsersInCity;

        /**
         * Load "users.txt", "relationships.txt", "countries.txt", "borders.txt" from the file system.
         * @param folderPath The path of the folder where to find the files.
         * @return Null iff all files were correctly loaded, the error message otherwise.
         */
        public String exercise1(final String folderPath) {
                final DataLoader data;

                try {
                        data = new DataLoader(folderPath + "users.txt", folderPath + "relationships.txt", folderPath + "countries.txt", folderPath + "borders.txt");
                } catch (LineException le) {
                        return "Error parsing CSV on file \"" + le.getFile() + "\", line " + le.getLine() + ": " + le.getMessage();
                } catch (IOException e) {
                        return "Error reading a file: " + e;
                }

                try {
                        p_friendshipGraph = data.getFriendshipGraph();
                        p_cityBorders = data.getCityBorders();
                } catch (LineException le ) {
                        return "Error interpreting file \"" + le.getFile() + "\", line " + le.getLine() + ": " + le.getMessage();
                }

                p_userCities = data.getUserCities();
                p_cityDistances = data.getCityDistances();
                p_numberOfUsersInCity = data.getNumberOfUsersInACity();
                return null;
        }

        /**
         * Find the common friends between the N most popular users.
         * @param N The number of popular users to consider.
         * @return The usernames of the common friends of the first N users.
         */
        public Set<String> exercise2(final int N) {
                return exercise2(N, p_userCities, p_friendshipGraph);
        }

        /**
         * Find the common friends between the N most popular users.
         * @param N The number of popular users to consider.
         * @param userCities A map that relates the username of a user (key) with the name of the city the user lives
         *      in (value).
         * @param friendshipGraph An undirected graph where the vertices represent the name of users and the weights
         *      represent whether the two user are friends (true) or not (null or false).
         * @return The usernames of the common friends of the first N users.
         */
        public static Set<String> exercise2(    final int N,
                                                final Map<String, String> userCities,
                                                final UndirectedMatrixGraph<String, Boolean> friendshipGraph) {
                // Get list of usernames
                final ArrayList<String> userNames = new ArrayList<>(userCities.keySet());

                // Find the N most popular users
                ArrayList<String> mostPopularUsers = new ArrayList<>(N);
                {
                        // Make a list of the users and their number of friends
                        ArrayList<Pair<String, Integer>> usersAndFriends = new ArrayList<>();
                        for (String user : userNames) {
                                usersAndFriends.add(new Pair<>(user, friendshipGraph.inDegree(user)));
                        }
                        usersAndFriends.trimToSize();
                        // Sort the list, put the most popular first
                        usersAndFriends.sort(new Comparator<Pair<String, Integer>>() {
                                @Override
                                public int compare(Pair<String, Integer> o1, Pair<String, Integer> o2) {
                                        return o2.getSecond() - o1.getSecond();
                                }
                        });
                        // Save the usernames of the most popular users
                        for (int i = 0; i < N && i < usersAndFriends.size(); i++) {
                                mostPopularUsers.add(usersAndFriends.get(i).getFirst());
                        }
                }

                // Find the usernames of the users who are friends with all the most popular users
                if(mostPopularUsers.size() == 0) return new HashSet<>();
                // Populate the friendsInCommon with the friends of the first popular user
                HashSet<String> friendsInCommon = new HashSet<>(friendshipGraph.outgoingEdges(mostPopularUsers.get(0)));

                for (int i = 1; i < mostPopularUsers.size(); i++) {
                        final String popularUseri = mostPopularUsers.get(i);
                        final Set<String> friendsOfPopularUseri = friendshipGraph.outgoingEdges(popularUseri);
                        friendsInCommon.removeIf(friendOfPopularUser0 -> !friendsOfPopularUseri.contains(friendOfPopularUser0));
                }

                return friendsInCommon;
        }

        /**
         * Find the minimum number of edges needed for every user in the network to be able to contact every other user.
         * @return Null iff the graph is not conex, the minimum number of edges needed for every user in the network to
         *      be able to contact every other user otherwise.
         */
        public Integer exercise3() {
                return exercise3(p_friendshipGraph);
        }

        /**
         * Find the minimum number of edges needed for every user in the network to be able to contact every other user.
         * @param friendshipGraph An undirected graph where the vertices represent the name of users and the weights
         *          represent whether the two user are friends (true) or not (null or false).
         * @return Null iff the graph is not conex, the minimum number of edges needed for every user in the network to
         *      be able to contact every other user otherwise.
         */
        public static Integer exercise3(final UndirectedMatrixGraph<String, Boolean> friendshipGraph) {
                // Get a list of all usernames
                final ArrayList<String> userNames = new ArrayList<>(friendshipGraph.vertices());
                // The maximum of the maximum number of edges between the firs user and every other reachable user
                int maxMax = 0;
                // Store the number of initial users
                final int usersN = userNames.size();

                for (int currentFirstUser = 0; currentFirstUser < usersN; currentFirstUser++) {
                        // Make a map that relates a user to whether it has been visited (true) or not (false)
                        HashMap<String, Boolean> visited = new HashMap<>();
                        // Initialize the map to false
                        for (String u : userNames) {
                                visited.put(u, false);
                        }

                        // Create a stack of the users we still need to visit
                        Stack<String> toVisit = new Stack<>();

                        // Place the first user in the stack of users to visit
                        toVisit.push(userNames.get(currentFirstUser));
                        // Mark the first user as being visited
                        visited.put(userNames.get(currentFirstUser), true);

                        // The maximum number of edges between the firs user and every other reachable user
                        int max = -1;

                        // While there are still users to visit
                        do {
                                // Increment max
                                max++;
                                // The users in toVisited were gathered by going trough "max" edges
                                // the new_toVisit users were gathered by going trough "max+1" edges
                                Stack<String> new_toVisit = new Stack<>();
                                // While there are still users to visit
                                while (!toVisit.empty()) {
                                        final String userToVisit = toVisit.pop();
                                        // Update max
                                        // For every friend of the user
                                        for (String friendOfUserToVisit : friendshipGraph.outgoingEdges(userToVisit)) {
                                                // If the user has not been visited
                                                if (!visited.get(friendOfUserToVisit)) {
                                                        // Add the user to the stack and mark them as visited
                                                        visited.put(friendOfUserToVisit, true);
                                                        new_toVisit.push(friendOfUserToVisit);
                                                }
                                        }
                                }
                                // Set up the next loop iteration
                                toVisit = new_toVisit;
                                // If the new_toVisit visit is empty, it means that there were no users where we needed
                                // to go trough "max+1" edges, therefore every user is reachable in at most "max" edges
                                // from the "currentFirstUser"
                        } while (!toVisit.empty());

                        if(currentFirstUser == 0) {
                                // Is the graph conex?
                                // We only need to check if the graph is conex once
                                for (String userName : visited.keySet()) {
                                        if (!visited.get(userName)) {
                                                // If any user was not reachable from the first user, then the graph is not conex
                                                return null;
                                        }
                                }
                                maxMax = max;
                        } else if (maxMax < max) maxMax = max; // Update maxMax
                }

                return maxMax;
        }

        /**
         * Find a user's friends that can be reached by going trough at most a certain number of borders.
         * @param user The user to consider.
         * @param maxNOfBorderCrossings The maximum number of border crossings to consider.
         * @return The user's friend who can be reached by going trough at most "maxNOfBorderCrossings" borders.
         */
        public Set<String> exercise4(final String user, final int maxNOfBorderCrossings) {
                return exercise4(user, maxNOfBorderCrossings, p_cityBorders, p_userCities, p_friendshipGraph);

        }

        /**
         * Find a user's friends that can be reached by going trough at most a certain number of borders.
         * @param user The user to consider.
         * @param maxNOfBorderCrossings The maximum number of border crossings to consider.
         * @param cityBorders An undirected graph where the vertices represent the cities and the weights represent whether the
         *      two cities share a border (true) or not (null or false).
         * @param userCities A map that relates the username of a user (key) with the name of the city the user
         *      lives in (value).
         * @param friendshipGraph An undirected graph where the vertices represent the name of users and the weights
         *      represent whether the two user are friends (true) or not (null or false).
         * @return The user's friend who can be reached by going trough at most "maxNOfBorderCrossings" borders.
         */
        public static Set<String> exercise4( final String user,
                                             final int maxNOfBorderCrossings,
                                             final UndirectedMapGraph<String, Boolean> cityBorders,
                                             final Map<String, String> userCities,
                                             final UndirectedMatrixGraph<String, Boolean> friendshipGraph) {

                // Relates the name of a city to whether it was visited
                HashMap<String, Boolean> visited = new HashMap<>();
                // Initialize the map
                for(String u : cityBorders.vertices()) {
                        visited.put(u, false);
                }

                // Create a stack of cities to visit
                Stack<String> toVisit = new Stack<>();

                // Create a set of the cities that can be reached by crossing at most "maxNOfBorderCrossings" borders
                HashSet<String> closeCities = new HashSet<>();

                // Put the user's city in the stack and mark it as visited
                toVisit.push(userCities.get(user));
                visited.put(userCities.get(user), true);
                closeCities.add(userCities.get(user));

                for (int bordersCrossed = 0; bordersCrossed < maxNOfBorderCrossings; bordersCrossed++) {
                        Stack<String> new_toVisit = new Stack<>();
                        while (!toVisit.empty()) {
                                final String visitingCity = toVisit.pop();
                                for(final String neighboringCity : cityBorders.outgoingEdges(visitingCity)) {
                                        if(!visited.get(neighboringCity)) {
                                                visited.put(neighboringCity, true);
                                                new_toVisit.push(neighboringCity);
                                                closeCities.add(neighboringCity);
                                        }
                                }
                        }
                        toVisit = new_toVisit;
                }

                // Friends of the user who live in the closeCities
                HashSet<String> closeFriends = new HashSet<>();

                for(String friend : friendshipGraph.outgoingEdges(user)) {
                        // If the friend's city is close the the user
                        if(closeCities.contains(userCities.get(friend))) {
                                // Save the user as a close friend
                                closeFriends.add(friend);
                        }
                }

                return closeFriends;
        }

        /**
         * Finds the n cities with more centrality.
         * @param userPercentage The minimum percentage of users in the city so that it can appear in the list.
         * @param numberOfCities The number of cities to try to find.
         * @return A list with at max "numberOfCities" cities.
         */
        public List<String> exercise5(final double userPercentage, final int numberOfCities) {
                return exercise5(userPercentage, numberOfCities, p_cityDistances, p_numberOfUsersInCity);
        }

        /**
         * Finds the n cities with more centrality.
         * @param userPercentage The minimum percentage of users in the city so that it can appear in the list.
         * @param numberOfCities The number of cities to try to find.
         * @param cityDistances An undirected graph where the vertices represent the cities and the weights represent
         *      the distance in Km between two cities.
         * @param numberOfUsersInCity A map that relates the name of a city (key) with the number of users who live in
         *      that city (value).
         * @return A list with at max "numberOfCities" cities.
         */
        public static List<String> exercise5( final double userPercentage,
                                              final int numberOfCities,
                                              final Graph<String, Double> cityDistances,
                                              final Map<String, Integer> numberOfUsersInCity) {

                // Create a list of all the cities and their mean distance to all other cities
                ArrayList<Pair<String, Double>> citiesMeanDistance = new ArrayList<>();
                // Populate the list
                // The total number of cities
                final double totalNumberOfCities = cityDistances.vertices().size();
                // For every city
                for(String city1 : cityDistances.vertices()) {
                        // Make the sum of its distance to every other city
                        double sum = 0;
                        for(String city2 : cityDistances.vertices()) {
                                sum += cityDistances.getEdge(city1, city2);
                        }
                        // Add the mean to the list
                        citiesMeanDistance.add(new Pair<>(city1, sum/totalNumberOfCities));
                }
                // Sort the list, the ones with the smaller mean come first
                citiesMeanDistance.sort(new Comparator<Pair<String, Double>>() {
                        @Override
                        public int compare(Pair<String, Double> o1, Pair<String, Double> o2) {
                                return o1.getSecond().compareTo(o2.getSecond());
                        }
                });

                // Calculate the total number of users
                double userCount = 0;
                for(String city : numberOfUsersInCity.keySet()) {
                        userCount += numberOfUsersInCity.get(city);
                }

                // Get cities with at least "userPercentage" of the users
                // Make an array to save the names of the cities
                ArrayList<String> citiesWithUPUsers = new ArrayList<>();
                final double userPercentageP = userPercentage / 100.0;
                // For each city and it's distance
                for(Pair<String, Double> city : citiesMeanDistance) {
                        final String cityName = city.getFirst();
                        // Calculate the percentage of users living in that city
                        final double percentage = ((double)numberOfUsersInCity.get(cityName)) / userCount;
                        // If city has at least "userPercentage" of the users
                        if(percentage >= userPercentageP) {
                                // Add the city to the list
                                citiesWithUPUsers.add(cityName);
                                // If the city already contains numberOfCities cities
                                if(citiesWithUPUsers.size() == numberOfCities) {
                                        // End the loop
                                        break;
                                }
                        }
                }
                return citiesWithUPUsers;
        }
}
