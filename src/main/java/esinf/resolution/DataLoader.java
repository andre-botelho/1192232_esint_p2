package esinf.resolution;

import esinf.graph.Graph;
import esinf.graph.UndirectedMapGraph;
import esinf.graph.UndirectedMatrixGraph;
import esinf.csvparser.FileCSV;
import esinf.util.LineException;
import esinf.util.Pair;

import java.io.IOException;
import java.util.*;

public class DataLoader {

        private final FileCSV usersCSV, relationshipsCSV, countriesCSV, bordersCSV;
        private final String relationshipsTXT, bordersTXT;

        /**
         * Constructor.
         * @param usersPath The path to the "users.txt" file.
         * @param relationshipsPath The path to the "relationships.txt" file.
         * @param countriesPath The path to the "countries.txt" file.
         * @param bordersPath The path to the "borders.txt" file.
         */
        public DataLoader(String usersPath, String relationshipsPath, String countriesPath, String bordersPath) throws IOException {
                final ArrayList<Pair<String, Integer>> filesAndWidths = new ArrayList<>();
                filesAndWidths.add(new Pair<>(usersPath, 3));
                filesAndWidths.add(new Pair<>(relationshipsPath, 2));
                filesAndWidths.add(new Pair<>(countriesPath, 6));
                filesAndWidths.add(new Pair<>(bordersPath, 2));
                int i = 0;
                FileCSV[] files = new FileCSV[filesAndWidths.size()];
                for(Pair<String, Integer> p : filesAndWidths) {
                        files[i++] = new FileCSV(p.getFirst(), "\n", ",", false, p.getSecond());
                }
                this.usersCSV = files[0];
                this.relationshipsCSV = files[1];
                this.countriesCSV = files[2];
                this.bordersCSV = files[3];
                this.bordersTXT = bordersPath;
                this.relationshipsTXT = relationshipsPath;
        }

        /**
         * @return An undirected graph where the vertices represent the name of users and the weights represent whether
         *      the two user are friends (true) or not (null or false).
         */
        public UndirectedMatrixGraph<String, Boolean> getFriendshipGraph() throws LineException {
                UndirectedMatrixGraph<String, Boolean> relations = new UndirectedMatrixGraph<>();
                final FileCSV users = usersCSV;
                final FileCSV relationships = relationshipsCSV;

                for (int l = 0; l < users.getHeight(); l++) {
                        final List<String> line = users.get(l);
                        final String user = line.get(0);
                        relations.insertVertex(user);
                        relations.insertEdge(user, user, true);
                }

                for (int l = 0; l < relationships.getHeight(); l++) {
                        final List<String> line = relationships.get(l);
                        final String user1 = line.get(0);
                        final String user2 = line.get(1);
                        try {
                                relations.insertEdge(user1, user2, true);
                        } catch (IllegalAccessError illegal) {
                                throw new LineException("The relationships file contains the user " + illegal.getMessage() + " that was not declared in the users file", this.relationshipsTXT, l+1);
                        }
                }
                return relations;
        }

        /**
         * @return A map that relates the username of a user (key) with the name of the city the user lives in (value).
         */
        public Map<String, String> getUserCities() {
                Map<String, String> userCity = new HashMap<>();
                final FileCSV users = usersCSV;

                for (int l = 0; l < users.getHeight(); l++) {
                        final List<String> line = users.get(l);
                        final String user = line.get(0);
                        final String city = line.get(2);
                        userCity.put(user, city);
                }

                return userCity;
        }

        /**
         * @return A map that relates the name of a city (key) with the number of users who live in that city (value).
         */
        public Map<String, Integer> getNumberOfUsersInACity () {
                Map<String, Integer> cityNUsers = new HashMap<>();
                final FileCSV users = usersCSV;

                for (int l = 0; l < users.getHeight(); l++) {
                        final List<String> line = users.get(l);
                        final String city = line.get(2);
                        if(!cityNUsers.containsKey(city)) cityNUsers.put(city, 1);
                        else                              cityNUsers.put(city, cityNUsers.get(city)+1);
                }

                return  cityNUsers;
        }

        /**
         * @return An undirected graph where the vertices represent the cities and the weights represent whether the
         *      two cities share a border (true) or not (null or false).
         */
        public UndirectedMapGraph<String, Boolean> getCityBorders () throws LineException {
                UndirectedMapGraph<String, Boolean> cityBorders = new UndirectedMapGraph<>();

                FileCSV countries = countriesCSV;
                FileCSV borders = bordersCSV;

                HashMap<String, String> countryCity = new HashMap<>();

                for (int l = 0; l < countries.getHeight(); l++) {
                        final List<String> line = countries.get(l);
                        final String country = line.get(0);
                        final String city = line.get(3);
                        countryCity.put(country, city);
                        cityBorders.insertVertex(city);
                        cityBorders.insertEdge(city, city, true);
                }

                for (int l = 0; l < borders.getHeight(); l++) {
                        List<String> line = borders.get(l);
                        final String country1 = line.get(0);
                        final String country2 = line.get(1);
                        for(final String k : new String[]{country1,country2})
                                if(!countryCity.containsKey(k))
                                        throw new LineException("The borders file contains the country " + k + " that was not declared in the countries file", this.bordersTXT, l+1);
                        cityBorders.insertEdge(countryCity.get(country1), countryCity.get(country2), true);
                }
                return  cityBorders;
        }

        /**
         * Calculate the distance between two points.
         * @param latitude1 The latitude of the first point.
         * @param longitude1 The longitude of the first point.
         * @param latitude2 The latitude of the second point.
         * @param longitude2 The longitude of the second point.
         * @return The distance in Km between the two points.
         */
        private static double distanceKM(double latitude1, double longitude1, double latitude2, double longitude2) {
                final double R = 6371e3; // metres
                final double lat1 = latitude1 * Math.PI/180; // lat, lon in radians
                final double lat2 = latitude2 * Math.PI/180;
                final double delta_lat = (latitude2-latitude1) * Math.PI/180;
                final double delta_lon = (longitude1-longitude2) * Math.PI/180;
                final double a = Math.pow(Math.sin(delta_lat/2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(delta_lon/2), 2);
                final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

                return (R * c)/1000; // in metres
        }

        /**
         * @return An undirected graph where the vertices represent the cities and the weights represent the distance
         *      in Km between two cities.
         */
        public Graph<String, Double> getCityDistances () {
                Graph<String, Double> cityDistances = new UndirectedMatrixGraph<>();

                FileCSV countries = countriesCSV;

                HashMap<String, Pair<Double, Double>> cityPositions = new HashMap<>();

                for (int l = 0; l < countries.getHeight(); l++) {
                        final List<String> line = countries.get(l);
                        final String city = line.get(3);
                        final double latitude = Double.parseDouble(line.get(4));
                        final double longitude = Double.parseDouble(line.get(5));
                        cityPositions.put(city, new Pair<>(latitude, longitude));
                        cityDistances.insertVertex(city);
                        cityDistances.insertEdge(city, city, 0.0);
                }

                for(String city1 : cityPositions.keySet()) {
                        final Pair<Double, Double> pos1 = cityPositions.get(city1);
                        for(String city2 : cityPositions.keySet()) {
                                final Pair<Double, Double> pos2 = cityPositions.get(city2);
                                final Double distance = distanceKM(pos1.getFirst(),pos1.getSecond(),pos2.getFirst(),pos2.getSecond());
                                cityDistances.insertEdge(city1, city2, distance);
                        }
                }

                return cityDistances;
        }
}
