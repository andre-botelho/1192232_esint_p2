package esinf.graph;

import java.util.*;

public class MatrixGraph<V, E> implements Graph<V, E> {

        Map<V, Integer> vertexIds;
        ArrayList<ArrayList<E>> vertices;
        int edgeN;

        public MatrixGraph() {
                this.edgeN = 0;
                this.vertices = new ArrayList<>();
                this.vertexIds = new HashMap<>();
        }

        /**
         * Helper function to access vertex IDs.
         * @param v The vertex to access.
         * @return The id of that vertex.
         */
        private int getId(V v) {
                try {
                        return vertexIds.get(v);
                } catch (NullPointerException e) {
                        throw new IllegalAccessError(v.toString());
                }
        }

        /**
         * Get the number of vertices. O(1)
         *
         * @return The number of vertices.
         */
        @Override
        public int numVertices() {
                return vertices.size();
        }

        /**
         * Get the number of edges. O(1)
         *
         * @return The number of edges.
         */
        @Override
        public int numEdges() {
                return edgeN;
        }

        /**
         * Get all vertices.
         * O(V) could be O(1) if returning the key-set.
         *
         * @return A list with all vertices of the graph.
         */
        @Override
        public Set<V> vertices() {
                return new HashSet<>(vertexIds.keySet());
        }

        /**
         * Get the weight of the edge v -> u. O(1)
         *
         * @param v Outgoing vertex.
         * @param u Incoming vertex.
         * @return The weight of the edge.
         */
        @Override
        public E getEdge(V v, V u) {
                return vertices.get(getId(v)).get(getId(u));
        }

        /**
         * O(V)
         * @param v The outgoing vertex.
         * @return The number of outgoing edges coming from v.
         */
        @Override
        public int outDegree(V v) {
                int degree = 0;
                for(E i : vertices.get(getId(v)))
                        if(i != null)
                                degree++;
                return degree;
        }

        /**
         * O(V)
         * @param v The incoming vertex.
         * @return The number of incoming edges coming into v.
         */
        @Override
        public int inDegree(V v) {
                int idx = getId(v);
                int degree = 0;
                for(List<E> i : vertices)
                        if(i.get(idx) != null)
                                degree++;
                return degree;
        }

        /**
         * @param v The outgoing vertex.
         * @return All the vertices u of the graph such that v -> u is an existing edge.
         */
        @Override
        public Set<V> outgoingEdges(V v) {
                HashSet<V> vert = new HashSet<>();
                List<E> l = vertices.get(getId(v));
                outer:
                for(int i = 0; i < l.size(); i++) {
                        if(l.get(i) != null) {
                                for(V u : vertexIds.keySet()) {
                                        int r = getId(u);
                                        if(r == i) {
                                                vert.add(u);
                                                continue outer;
                                        }
                                }
                        }
                }
                return vert;
        }

        /**
         * @param v The incoming vertex.
         * @return All the vertices u of the graph such that u -> v is an existing edge.
         */
        @Override
        public Set<V> incomingEdges(V v) {
                HashSet<V> vert = new HashSet<>();
                int idx = getId(v);
                outer:
                for(int i = 0; i < vertices.size(); i++) {
                        if(vertices.get(i).get(idx) != null) {
                                for(V u : vertexIds.keySet()) {
                                        int r = getId(u);
                                        if(r == i) {
                                                vert.add(u);
                                                continue outer;
                                        }
                                }
                        }
                }
                return vert;
        }

        /**
         * O(V^2)
         * Add a new vertex into the graph.
         *
         * @param v The vertex to add.
         * @return True iff the vertex v was inserted into the graph.
         */
        @Override
        public boolean insertVertex(V v) {
                if(vertexIds.containsKey(v)) {
                        return false;
                } else {
                        int newSize = vertices.size()+1;
                        vertexIds.put(v, newSize-1);
                        for(List<E> l : vertices)
                                l.add(null);
                        ArrayList<E> list = new ArrayList<>();
                        for(int i = 0; i < newSize; i++) list.add(null);
                        vertices.add(list);
                        return true;
                }
        }

        /**
         * Remove a vertex from the graph. O(V)
         *
         * @param v The vertex to remove.
         * @return True iff the vertex v was removed into the graph.
         * @throws IllegalAccessError In case v is not a valid vertex.
         */
        @Override
        public boolean removeVertex(V v) {
                for(V u : vertexIds.keySet()) {
                        this.removeEdge(u,v);
                        this.removeEdge(v,u);
                }

                final int indx = getId(v);

                vertices.remove(indx);
                for(List<E> l : vertices)
                        l.remove(indx);

                vertexIds.remove(v);
                for(V u : vertexIds.keySet()) {
                        final int id = getId(u);
                        if(id > indx) vertexIds.put(u, id-1);
                }
                return true;
        }

        /**
         * Add an edge to the graph. O(1)
         *
         * @param v Outgoing vertex.
         * @param u Incoming edge.
         * @param x Weight of the edge.
         * @return True iff the edge was added into the graph.
         */
        @Override
        public boolean insertEdge(V v, V u, E x) {
                List<E> l = vertices.get(getId(v));
                int indx = getId(u);
                E ii = l.get(indx);
                l.set(indx, x);
                if(ii == null) {
                        edgeN++;
                        return true;
                } else {
                        return false;
                }
        }

        /**
         * Remove edge from graph. O(1)
         *
         * @param v Incoming edge.
         * @param u Outgoing edge.
         * @return True iff the edge was removed.
         */
        @Override
        public boolean removeEdge(V v, V u) {
                List<E> l = vertices.get(getId(v));
                int indx = getId(u);
                E ii = l.get(indx);
                if(ii == null) {
                        return false;
                } else {
                        l.set(indx, null);
                        edgeN--;
                        return true;
                }
        }
}
