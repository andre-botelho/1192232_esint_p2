package esinf.graph;

import java.util.Set;

public interface Graph<V, E> {

        /**
         * Get the number of vertices.
         * @return The number of vertices.
         */
        int numVertices();

        /**
         * Get the number of edges.
         * @return The number of edges.
         */
        int numEdges();

        /**
         * Get all vertices.
         * @return A list with all vertices of the graph.
         */
        Set<V> vertices();

        /**
         * Get the weight of the edge v -> u.
         * @param v Outgoing vertex.
         * @param u Incoming vertex.
         * @return The weight of the edge.
         */
        E getEdge(V v, V u);

        /**
         * @param v The outgoing vertex.
         * @return The number of outgoing edges coming from v.
         */
        int outDegree(V v);

        /**
         * @param v The incoming vertex.
         * @return The number of incoming edges coming into v.
         */
        int inDegree(V v);

        /**
         * @param v The outgoing vertex.
         * @return All the vertices u of the graph such that v -> u is an existing edge.
         */
        Set<V> outgoingEdges(V v);

        /**
         * @param v The incoming vertex.
         * @return All the vertices u of the graph such that u -> v is an existing edge.
         */
        Set<V> incomingEdges(V v);

        /**
         * Add a new vertex into the graph.
         * @param v The vertex to add.
         * @return True iff the vertex v was inserted into the graph.
         */
        boolean insertVertex(V v);

        /**
         * Remove a vertex from the graph.
         * @param v The vertex to remove.
         * @return True iff the vertex v was removed into the graph.
         * @throws IllegalAccessError In case v is not a valid vertex.
         */
        boolean removeVertex(V v);

        /**
         * Add an edge to the graph.
         * @param v Outgoing vertex.
         * @param u Incoming edge.
         * @param x Weight of the edge.
         * @return True iff the edge was added into the graph.
         */
        boolean insertEdge(V v, V u, E x);

        /**
         * Remove edge from graph.
         * @param v Incoming edge.
         * @param u Outgoing edge.
         * @return True iff the edge was removed.
         */
        boolean removeEdge(V v, V u);
}
