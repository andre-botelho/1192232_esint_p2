package esinf.graph;

import java.util.*;

public class MapGraph<V, E> implements Graph<V, E> {

        Map<V, Map<V, E>> vertices;
        int edgeCount;

        public MapGraph() {
                this.vertices = new HashMap<>();
                this.edgeCount = 0;
        }

        /**
         * Helper function to access vertex maps.
         * @param v The vertex to access.
         * @return The map of that vertex.
         */
        private Map<V, E> get(V v) {
                        Map<V, E> m = vertices.get(v);
                        if(m == null) throw new IllegalAccessError(v.toString());
                        else return m;
        }

        /**
         * Helper function to access edges of maps.
         * @param m The map to access.
         * @param v The vertex to access.
         * @return The edge of that vertex.
         */
        private E get(Map<V, E> m, V v) {
                if(!vertices.containsKey(v)) throw new IllegalAccessError(v.toString());
                return m.get(v);
        }


        /**
         * Get the number of vertices. O(1)
         *
         * @return The number of vertices.
         */
        @Override
        public int numVertices() {
                return vertices.size();
        }

        /**
         * Get the number of edges. O(1)
         *
         * @return The number of edges.
         */
        @Override
        public int numEdges() {
                return edgeCount;
        }

        /**
         * Get all vertices.
         * O(V) could be O(1) if returning the key-set.
         *
         * @return A list with all vertices of the graph.
         */
        @Override
        public Set<V> vertices() {
                return new HashSet<>(vertices.keySet());
        }

        /**
         * Get the weight of the edge v -> u. O(1)
         *
         * @param v Outgoing vertex.
         * @param u Incoming vertex.
         * @return The weight of the edge.
         */
        @Override
        public E getEdge(V v, V u) {
                return get(get(v), u);
        }

        /**
         * O(1)
         * @param v The outgoing vertex.
         * @return The number of outgoing edges coming from v.
         */
        @Override
        public int outDegree(V v) {
                return get(v).size();
        }

        /**
         * O(V)
         * @param v The incoming vertex.
         * @return The number of incoming edges coming into v.
         */
        @Override
        public int inDegree(V v) {
                int degree = 0;
                for(V u : vertices.keySet()) {
                        if(get(u).containsKey(v))
                                degree++;
                }
                return degree;
        }

        /**
         * O(V) could be O(1) if returning keySet directly.
         * @param v The outgoing vertex.
         * @return All the vertices u of the graph such that v -> u is an existing edge.
         */
        @Override
        public Set<V> outgoingEdges(V v) {
                return new HashSet<>(get(v).keySet());
        }

        /**
         * O(V)
         * @param v The incoming vertex.
         * @return All the vertices u of the graph such that u -> v is an existing edge.
         */
        @Override
        public Set<V> incomingEdges(V v) {
                HashSet<V> list = new HashSet<>();
                for(V u : vertices.keySet()) {
                        if(get(u).containsKey(v))
                                list.add(u);
                }
                return list;
        }

        /**
         * O(1)
         * Add a new vertex into the graph.
         *
         * @param v The vertex to add.
         * @return True iff the vertex v was inserted into the graph.
         */
        @Override
        public boolean insertVertex(V v) {
                if(vertices.containsKey(v)) {
                        return false;
                } else {
                        vertices.put(v, new HashMap<>());
                        return true;
                }
        }

        /**
         * Remove a vertex from the graph. O(V)
         *
         * @param v The vertex to remove.
         * @return True iff the vertex v was removed into the graph.
         * @throws IllegalAccessError In case v is not a valid vertex.
         */
        @Override
        public boolean removeVertex(V v) throws IllegalAccessError {
                if(!vertices.containsKey(v)) throw new IllegalAccessError(v.toString());
                edgeCount -= vertices.get(v).size();
                vertices.remove(v);
                for(V u : vertices.keySet()) {
                        if(get(u).remove(v) != null)
                                edgeCount--;
                }
                return true;
        }

        /**
         * Add an edge to the graph. O(1)
         *
         * @param v Outgoing vertex.
         * @param u Incoming edge.
         * @param x Weight of the edge.
         * @return True iff the edge was added into the graph.
         */
        @Override
        public boolean insertEdge(V v, V u, E x) {
                Map<V, E> m = get(v);
                final boolean isNewEdge = !m.containsKey(u);
                if(isNewEdge) {
                        if(!vertices.containsKey(u)) throw new IllegalAccessError(v.toString());
                        edgeCount++;
                }
                m.put(u, x);
                return isNewEdge;
        }

        /**
         * Remove edge from graph. O(1)
         *
         * @param v Incoming edge.
         * @param u Outgoing edge.
         * @return True iff the edge was removed.
         */
        @Override
        public boolean removeEdge(V v, V u) {
                Map<V, E> m = get(v);
                if(m.containsKey(u)) {
                        m.remove(u);
                        edgeCount--;
                        return true;
                } else {
                        return false;
                }
        }
}
