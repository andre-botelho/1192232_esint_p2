package esinf.graph;

public class UndirectedMapGraph<V, E> extends MapGraph<V, E> {

        /**
         * Add an edge to the graph.
         *
         * @param v Outgoing vertex.
         * @param u Incoming edge.
         * @param x Weight of the edge.
         * @return True iff the edge was added into the graph.
         */
        @Override
        public boolean insertEdge(V v, V u, E x) {
                boolean b = super.insertEdge(v,u,x);
                b = super.insertEdge(u,v,x) || b;
                return b;
        }

        /**
         * Remove edge from graph.
         *
         * @param v Incoming edge.
         * @param u Outgoing edge.
         * @return True iff the edge was removed.
         */
        @Override
        public boolean removeEdge(V v, V u) {
                boolean b = super.removeEdge(v,u);
                b = super.removeEdge(u,v) || b;
                return b;
        }
}
