package esinf.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MatrixGraphTest {

        MatrixGraph<Integer, String> m;
        List<Integer> listOfVertices;
        final int i1 = 5;
        final int i2 = 10;

        @BeforeEach
        void setUp() {
                m = new MatrixGraph<>();
                listOfVertices = new ArrayList<>();
                for(int i = 0; i < Math.max(i1,i2); i++) {
                        m.insertVertex(i);
                        listOfVertices.add(i);
                }
                for(int i = 0; i < i1; i++)
                        for(int j = 0; j < i2; j++)
                                m.insertEdge(i, j, String.valueOf(i-j));
        }

        @Test
        void numVertices() {
                assertEquals(listOfVertices.size(), m.numVertices());
        }

        @Test
        void numEdges() {
                assertEquals(i1*i2, m.numEdges());
        }

        @Test
        void vertices() {
                assertTrue(m.vertices().containsAll(listOfVertices));
                assertEquals(listOfVertices.size(), m.vertices().size());
        }

        @Test
        void getEdge() {
                for(int i = 0; i < i1; i++)
                        for(int j = 0; j < i2; j++)
                                assertEquals(String.valueOf(i-j), m.getEdge(i, j));
        }

        @Test
        void outDegree() {
                assertEquals(10, m.outDegree(0));
                assertEquals(0, m.outDegree(9));
        }

        @Test
        void inDegree() {
                assertEquals(5, m.inDegree(0));
                assertEquals(5, m.inDegree(9));
        }

        @Test
        void outgoingEdges() {
                assertTrue(m.outgoingEdges(0).containsAll(listOfVertices));
                assertEquals(0, m.outgoingEdges(9).size());
        }

        @Test
        void incomingEdges() {
                assertTrue(m.incomingEdges(0).containsAll(Arrays.asList(0, 1, 2, 3, 4)));
                assertTrue(m.incomingEdges(9).containsAll(Arrays.asList(0, 1, 2, 3, 4)));
        }

        @Test
        void insertVertex() {
                assertFalse(m.insertVertex(0));
                assertTrue(m.insertVertex(66));
                listOfVertices.add(66);
                assertTrue(m.vertices().containsAll(listOfVertices));
                assertEquals(listOfVertices.size(), m.vertices().size());
        }

        @Test
        void removeVertex() {
                assertTrue(m.removeVertex(9));

                IllegalAccessError error = null;
                try {
                        assertFalse(m.removeVertex(9));
                } catch(IllegalAccessError e) {
                        error = e;
                }
                assertNotNull(error);

                listOfVertices.remove(9);
                assertTrue(m.vertices().containsAll(listOfVertices));
                assertEquals(listOfVertices.size(), m.vertices().size());
                assertEquals(45, m.numEdges());
        }

        @Test
        void insertEdge() {
                assertEquals(50, m.numEdges());
                assertTrue(m.insertEdge(8,7,"-1"));
                assertEquals("-1", m.getEdge(8,7));
                assertFalse(m.insertEdge(8,7,"1"));
                assertEquals("1", m.getEdge(8,7));
                assertEquals(51, m.numEdges());
        }

        @Test
        void removeEdge() {
                assertEquals(50, m.numEdges());
                assertTrue(m.removeEdge(1,2));
                assertFalse(m.removeEdge(1,2));
                assertEquals(49, m.numEdges());
        }
}