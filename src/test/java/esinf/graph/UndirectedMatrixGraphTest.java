package esinf.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UndirectedMatrixGraphTest {

        UndirectedMatrixGraph<String, Integer> m;

        @BeforeEach
        void setUp() {
                m = new UndirectedMatrixGraph<>();
                m.insertVertex("1");
                m.insertVertex("2");
                m.insertVertex("3");
                m.insertVertex("4");
        }

        @Test
        void insertEdge() {
                assertTrue(m.insertEdge("1", "1", 1));
                assertTrue(m.insertEdge("1", "2", 2));
                assertTrue(m.insertEdge("1", "3", 3));
                assertTrue(m.insertEdge("1", "4", 4));

                assertFalse(m.insertEdge("1","1", 1));
                assertFalse(m.insertEdge("2","1", 2));
                assertFalse(m.insertEdge("3","1", 3));
                assertFalse(m.insertEdge("4","1", 4));

                assertEquals(1, m.getEdge("1", "1"));
                assertEquals(2, m.getEdge("1", "2"));
                assertEquals(3, m.getEdge("1", "3"));
                assertEquals(4, m.getEdge("1", "4"));
                assertEquals(1, m.getEdge("1","1"));
                assertEquals(2, m.getEdge("2","1"));
                assertEquals(3, m.getEdge("3","1"));
                assertEquals(4, m.getEdge("4","1"));
        }

        @Test
        void removeEdge() {
                m.insertEdge("1", "1", 1);
                m.insertEdge("1", "2", 2);
                m.insertEdge("1", "3", 3);
                m.insertEdge("1", "4", 4);

                assertEquals(7, m.numEdges());
                assertTrue(m.removeEdge("1", "2"));
                assertEquals(7-2, m.numEdges());
                assertFalse(m.removeEdge("2", "1"));
                assertEquals(7-2, m.numEdges());

        }
}