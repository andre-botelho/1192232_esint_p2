package esinf.util;

import esinf.graph.Graph;
import esinf.graph.UndirectedMapGraph;
import esinf.graph.UndirectedMatrixGraph;
import esinf.resolution.DataLoader;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class DataLoaderTest {

        final String network = "src/test/resources/test-network/";
        final String users = network + "users";
        final String relationships = network + "relationships";
        final String countries = network + "countries";
        final String borders = network + "borders";
        final List<String> listOfCityNames = Arrays.asList("c0", "c1", "c2", "c3", "c4", "c5", "c1l", "c1r");

        @Test
        void doesNotThrowOnCorrectlyFormatedFiles() {
                Exception exception = null;
                try {
                        new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+".txt");
                } catch (Exception e) {
                        exception = e;
                }
                assertNull(exception);
        }


        LineException catchLineExceptionOnOpen(final String u, final String r, final String c, final String b) throws IOException {
                LineException exception = null;
                try {
                        new DataLoader(u,r,c,b);
                } catch (LineException e) {
                        exception = e;
                }
                return exception;
        }

        // Missing elements

        @Test
        void throwsOnMissingElementInUsersFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+"_missing_5.txt", relationships+".txt", countries+".txt", borders+".txt");

                assertNotNull(exception);
                assertEquals(users+"_missing_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnMissingElementInRelationshipsFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+".txt", relationships+"_missing_5.txt", countries+".txt", borders+".txt");

                assertNotNull(exception);
                assertEquals(relationships+"_missing_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnMissingElementInCountriesFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+".txt", relationships+".txt", countries+"_missing_5.txt", borders+".txt");

                assertNotNull(exception);
                assertEquals(countries+"_missing_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnMissingElementInBordersFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+".txt", relationships+".txt", countries+".txt", borders+"_missing_5.txt");

                assertNotNull(exception);
                assertEquals(borders+"_missing_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        // Extra elements

        @Test
        void throwsOnExtraElementInUsersFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+"_extra_5.txt", relationships+".txt", countries+".txt", borders+".txt");

                assertNotNull(exception);
                assertEquals(users+"_extra_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnExtraElementInRelationshipsFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+".txt", relationships+"_extra_5.txt", countries+".txt", borders+".txt");

                assertNotNull(exception);
                assertEquals(relationships+"_extra_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnExtraElementInCountriesFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+".txt", relationships+".txt", countries+"_extra_5.txt", borders+".txt");

                assertNotNull(exception);
                assertEquals(countries+"_extra_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnExtraElementInBordersFile() throws IOException {
                LineException exception = catchLineExceptionOnOpen(users+".txt", relationships+".txt", countries+".txt", borders+"_extra_5.txt");

                assertNotNull(exception);
                assertEquals(borders+"_extra_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        // Erroneous files
        @Test
        void throwsOnNonExistentUserInRelationshipsFile() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+"_non_existing_user_5.txt", countries+".txt", borders+".txt");

                LineException exception = null;
                try {
                        data.getFriendshipGraph();
                } catch (LineException e) {
                        exception = e;
                }
                assertNotNull(exception);
                assertEquals(relationships+"_non_existing_user_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        @Test
        void throwsOnNonExistentCountryInBordersFile() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+"_non_existing_country_5.txt");

                LineException exception = null;
                try {
                        data.getCityBorders();
                } catch (LineException e) {
                        exception = e;
                }

                assertNotNull(exception);
                assertEquals(borders+"_non_existing_country_5.txt", exception.getFile());
                assertEquals(5, exception.getLine());
        }

        // Graphs
        // In the test set:
        /*
                Countries:
                                 +----------------+
                                 | c0             |
                                 | population: 5  |
                                 | lat: 1         |
                                 | long: 0        |
                +----------------+----------------+----------------+
                | c1l            | c1             | c1r            |
                | population: 6  | population: 5  | population: 6  |
                | lat: 0         | lat: 1         | lat: 2         |
                | long: 1        | long: 1        | long: 1        |
                +----------------+----------------+----------------+
                                 | c1             |
                                 | population: 4  |
                                 | lat: 1         |
                                 | long: 4        |
                                 +----------------+
                                 | c1             |
                                 | population: 3  |
                                 | lat: 1         |
                                 | long: 8        |
                                 +----------------+
                                 | c1             |
                                 | population: 2  |
                                 | lat: 1         |
                                 | long: 16       |
                                 +----------------+
                                 | c1             |
                                 | population: 1  |
                                 | lat: 1         |
                                 | long: 32       |
                                 +----------------+
                Users:
                        c0:  u0 -u4
                        c1:  u5 -u9
                        c2:  u10-u13
                        c3:  u14-u16
                        c4:  u17-u18
                        c5:  u19
                        c1l: u20-u25
                        c1r: u26-u31

               Friendships:

                        u0-----+-----+----+
                        |      |     |    |
                        u1    u11   u21--u31
                        |      |     |
                        u2    u12   u22
                        |      |     |
                        u3    u13   u23
                        |      |     |
                        u4    u14   u24
                        |      |     |
                        u5    u15   u25
                        |      |     |
                        u6    u16   u26
                        |      |     |
                        u7    u17   u27
                        |      |     |
                        u8    u18   u28
                        |      |     |
                        u9    u19   u29
                        |      |     |
                        u10   u20   u30
         */

        @Test
        void getFriendshipGraph() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+".txt");
                UndirectedMatrixGraph<String, Boolean> friendships = data.getFriendshipGraph();
                // Read all vertices correctly
                Set<String> users = friendships.vertices();
                assertEquals(32, users.size());
                for(int i = 0; i < 32; i++)
                        assertTrue(users.contains("u"+i));
                // Everyone is friends with themselves
                for(String user : users) {
                        assertEquals(true, friendships.getEdge(user, user));
                }
                // Can read friendships correctly
                for(int i = 0; i < 31; i++) {
                        for(int j = i+1; j < 32; j++) {
                                final String ui = "u"+i;
                                final String uj = "u"+j;
                                if(i!=10 && i!=20 && i!=30 && i==j-1) {
                                        assertEquals(true, friendships.getEdge(ui, uj));
                                        assertEquals(true, friendships.getEdge(uj, ui));
                                } else if(i == 0 && (j == 11 || j == 21 || j == 31)) {
                                        assertEquals(true, friendships.getEdge(ui, uj));
                                        assertEquals(true, friendships.getEdge(uj, ui));
                                } else if(i==21 && j == 31) {
                                        assertEquals(true, friendships.getEdge(ui, uj));
                                        assertEquals(true, friendships.getEdge(uj, ui));
                                } else {
                                        assertEquals(null, friendships.getEdge(ui, uj));
                                        assertEquals(null, friendships.getEdge(uj, ui));
                                }
                        }
                }
        }

        @Test
        void getUserCities() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+".txt");
                Map<String, String> userCity = data.getUserCities();
                for(int i = 0; i <= 4; i++) assertEquals("c0", userCity.get("u"+i));
                for(int i = 5; i <= 9; i++) assertEquals("c1", userCity.get("u"+i));
                for(int i = 10; i <= 13; i++) assertEquals("c2", userCity.get("u"+i));
                for(int i = 14; i <= 16; i++) assertEquals("c3", userCity.get("u"+i));
                for(int i = 17; i <= 18; i++) assertEquals("c4", userCity.get("u"+i));
                assertEquals("c5", userCity.get("u19"));
                for(int i = 20; i <= 25; i++) assertEquals("c1l", userCity.get("u"+i));
                for(int i = 26; i <= 31; i++) assertEquals("c1r", userCity.get("u"+i));
        }

        @Test
        void getNumberOfUsersInACity() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+".txt");
                Map<String, Integer> cityNumber = data.getNumberOfUsersInACity();
                assertEquals(listOfCityNames.size(), cityNumber.keySet().size());
                assertEquals(5, cityNumber.get("c0"));
                assertEquals(5, cityNumber.get("c1"));
                assertEquals(4, cityNumber.get("c2"));
                assertEquals(3, cityNumber.get("c3"));
                assertEquals(2, cityNumber.get("c4"));
                assertEquals(1, cityNumber.get("c5"));
                assertEquals(6, cityNumber.get("c1l"));
                assertEquals(6, cityNumber.get("c1r"));
        }

        @Test
        void getCityBorders() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+".txt");
                UndirectedMapGraph<String, Boolean> cityBorders = data.getCityBorders();
                assertEquals(listOfCityNames.size(), cityBorders.vertices().size());
                assertTrue(cityBorders.vertices().containsAll(listOfCityNames));

                HashMap<String, HashSet<String>> borders = new HashMap<>();
                borders.put("c0",  new HashSet<>(Arrays.asList("c0",  "c1")));
                borders.put("c1",  new HashSet<>(Arrays.asList("c1",  "c0", "c1l", "c1r", "c2")));
                borders.put("c2",  new HashSet<>(Arrays.asList("c2",  "c1", "c3")));
                borders.put("c3",  new HashSet<>(Arrays.asList("c3",  "c2", "c4")));
                borders.put("c4",  new HashSet<>(Arrays.asList("c4",  "c3", "c5")));
                borders.put("c5",  new HashSet<>(Arrays.asList("c5",  "c4")));
                borders.put("c1l", new HashSet<>(Arrays.asList("c1l", "c1")));
                borders.put("c1r", new HashSet<>(Arrays.asList("c1r", "c1")));
                for(String c1 : cityBorders.vertices()) {
                        for(String c2 : cityBorders.vertices()) {
                                if(borders.get(c1).contains(c2)) {
                                        assertEquals(true, cityBorders.getEdge(c1,c2));
                                } else assertNull(cityBorders.getEdge(c1, c2));
                        }
                }
        }

        @Test
        void getCityDistances() throws IOException {
                DataLoader data = new DataLoader(users+".txt", relationships+".txt", countries+".txt", borders+".txt");
                Graph<String, Double> cityDistances = data.getCityDistances();
                assertEquals(listOfCityNames.size(), cityDistances.vertices().size());
                assertTrue(cityDistances.vertices().containsAll(listOfCityNames));
                for(String c : listOfCityNames) {
                        assertEquals(0.0, cityDistances.getEdge(c, c));
                }
        }
}