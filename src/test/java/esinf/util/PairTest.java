package esinf.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairTest {

        Pair<String, Integer> pair;

        @BeforeEach
        void setUp() {
                pair = new Pair<>("Um", 2);
        }

        @Test
        void getFirst() {
                assertEquals("Um", pair.getFirst());
        }

        @Test
        void getSecond() {
                assertEquals(2, pair.getSecond());
        }

        @Test
        void testToString() {
                assertEquals("[Um, 2]", pair.toString());
        }
}