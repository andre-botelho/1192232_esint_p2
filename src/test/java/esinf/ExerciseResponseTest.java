package esinf;

import esinf.resolution.ExerciseResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ExerciseResponseTest {

        final String test_network = "src/test/resources/test-network/";
        final String big_network = "src/test/resources/big-network/";
        final String small_network = "src/test/resources/small-network/";

        ExerciseResponse test;
        ExerciseResponse big;
        ExerciseResponse small;

        @BeforeEach
        void setUp() {
                test = new ExerciseResponse();
                big = new ExerciseResponse();
                small = new ExerciseResponse();
        }

        @Test
        void exercise1() {
                assertNull(test.exercise1(test_network));
                assertNull(big.exercise1(big_network));
                assertEquals(
                        "Error interpreting file \"src/test/resources/small-network/relationships.txt\", line 7: The relationships file contains the user u8 that was not declared in the users file",
                        small.exercise1(small_network)
                );
        }

        @Test
        void exercise2() {
                test.exercise1(test_network);
                big.exercise1(big_network);
                Set<String> st = test.exercise2(2);
                Set<String> sb = big.exercise2(5);
                assertEquals(3, st.size());
                assertEquals(8, sb.size());
                assertTrue(st.containsAll(Arrays.asList("u31", "u21", "u0")));
                assertTrue(sb.containsAll(Arrays.asList("u1", "u534", "u691", "u687", "u559", "u591", "u487", "u146")));

        }

        @Test
        void exercise3() {
                test.exercise1(test_network);
                big.exercise1(big_network);
                assertEquals(20, test.exercise3());
                assertNull(big.exercise3());
        }

        @Test
        void exercise4() {
                test.exercise1(test_network);
                big.exercise1(big_network);
                Set<String> st = test.exercise4("u0", 2);
                Set<String> sb = big.exercise4("u1", 1);
                assertEquals(5, st.size());
                assertEquals(9, sb.size());
                assertTrue(st.containsAll(Arrays.asList("u31", "u11", "u21", "u0", "u1")));
                assertTrue(sb.containsAll(Arrays.asList("u309", "u479", "u436", "u666", "u477", "u224", "u268", "u592", "u1")));
        }

        @Test
        void exercise5() {
                test.exercise1(test_network);
                big.exercise1(big_network);
                List<String> st = test.exercise5(15, 3);
                List<String> sb = big.exercise5(1, 5);
                assertEquals(3, st.size());
                assertEquals(5, sb.size());
                assertTrue(st.containsAll(Arrays.asList("c1", "c1r", "c1l")));
                assertTrue(sb.containsAll(Arrays.asList("liubliana", "vaduz", "viena", "zagreb", "praga")));
        }
}