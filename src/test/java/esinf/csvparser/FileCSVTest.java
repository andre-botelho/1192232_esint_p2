package esinf.csvparser;

import esinf.util.LineException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FileCSVTest {

        final String network = "src/test/resources/test-network/";

        @Test
        void opensFileCorrectly() throws IOException {
                FileCSV f;
                f = new FileCSV(network+"countries.txt", "\n", ",", false, 6);
        }

        @Test
        void readingAllLinesOfAFile() throws IOException {
                FileCSV f;
                f = new FileCSV(network+"countries.txt", "\n", ",", false, 6);
                assertEquals(8, f.getHeight());
        }

        @Test
        void correctlyReadingLine() throws IOException {
                FileCSV f;
                f = new FileCSV(network+"countries.txt", "\n", ",", false, 6);
                List<String> actual = f.get(4);
                String[] expected = {"c4", "xxx", "2", "c4", "1", "8"};
                assertIterableEquals(Arrays.asList(expected), actual);
        }

        @Test
        void throwsOnExtraElementOnLine() throws IOException {
                LineException exeption = null;
                try {
                        new FileCSV(network+"countries_extra_5.txt", "\n", ",", false, 6);
                } catch (LineException le) {
                        exeption = le;
                }
                assertNotNull(exeption);
                assertEquals(network+"countries_extra_5.txt", exeption.getFile());
                assertEquals(5, exeption.getLine());
        }

        @Test
        void throwsOnMissingElementOnLine() throws IOException {
                LineException exeption = null;
                try {
                        new FileCSV(network+"countries_extra_5.txt", "\n", ",", false, 6);
                } catch (LineException le) {
                        exeption = le;
                }
                assertNotNull(exeption);
                assertEquals(network+"countries_extra_5.txt", exeption.getFile());
                assertEquals(5, exeption.getLine());
        }
}